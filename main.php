<?php

namespace HelloWorld;

use HelloWorld\Lib\Common;

class Main {
    public function show() {
        $common = new Common();
        $common->say();
    }
}
